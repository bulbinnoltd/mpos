package com.example.salesdept.item

class SalesOrderItem(
    val id:Int,
    val tel: Int,
    val address: String,
    val contact_person: String,
    val delivery_time: String,
    val long_Id: String,
    val district :String,
    val complete_date: String,
    val remarks: String,
    val order_detail : ArrayList<String>?

)