package com.example.salesdept.item

class OrderItem(
    val id:Int,
    val name: String,
    val quantity: Int,
    val price: Double
)
