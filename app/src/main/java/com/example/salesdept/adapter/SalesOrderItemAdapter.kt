package com.example.salesdept.adapter

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.commit
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.salesdept.MainActivity
import com.example.salesdept.R
import com.example.salesdept.item.SalesOrderItem
import com.example.salesdept.ui.salesorder.BottomSheet
import com.example.salesdept.ui.salesorder.EditSalesOrderItemsFragment
import com.example.salesdept.ui.salesorder.SalesorderFragment
import com.example.salesdept.ui.salesorder.ViewSalesOrderItemsFragment


class SalesOrderItemAdapter (private val  mSalesOrderItems: List<SalesOrderItem>,private val parentFragment: Fragment) : RecyclerView.Adapter<SalesOrderItemAdapter.ViewHolder>()
{
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val contract_title = itemView.findViewById<TextView>(R.id.contract_title)
        val contract_desc = itemView.findViewById<TextView>(R.id.contract_desc)
        val contract_amount_value = itemView.findViewById<TextView>(R.id.contract_amount_value)
        val contract_card = itemView.findViewById<CardView>(R.id.contract_card)
        val contract_item_number = itemView.findViewById<TextView>(R.id.contract_item_number)
        val order_option_button = itemView.findViewById<ImageView>(R.id.order_option_icon)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_salesorder, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        viewHolder.setIsRecyclable(false)
        val contractitem: SalesOrderItem = mSalesOrderItems.get(position)
        // Set item views based on your views and data model
        val contract_title = viewHolder.contract_title
        val contract_desc = viewHolder.contract_desc
        val contract_amount_value = viewHolder.contract_amount_value

        val contract_card = viewHolder.contract_card
        val contract_item_number = viewHolder.contract_item_number

        val order_option_button = viewHolder.order_option_button

        order_option_button.setOnClickListener(){
//            Log.d("note", "button clicked")
            val args = Bundle()
            args.putString("key", "value")
            val bottomSheet = BottomSheet()
            bottomSheet.setArguments(args)
            (parentFragment.activity as MainActivity?)?.let { it1 ->
                bottomSheet.show(
                    it1.supportFragmentManager,
                    bottomSheet.getTag()
                )
            }

        }



        contract_title.setText( "訂單編號#".plus(contractitem.id)!!)
        contract_desc.setText(contractitem.address)
        contract_amount_value.setText( contractitem.tel.toString())


        val contract_item_number_value = position +1
        contract_item_number.setText(contractitem.district)

        contract_card.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_salesorderFragment_to_viewsalesorderFragement))

//        contract_card.setOnClickListener(View.OnClickListener { arg0 ->
//            Log.d("note", "CARD clicked")
//
//            val bundle = Bundle()
//            bundle.putString("contract_title", contractitem.long_Id)
//            bundle.putInt("status_rgb", contractitem.id)
//            bundle.putString("status", contractitem.long_Id)
//            bundle.putString("remarks", contractitem.remarks)
//            bundle.putString("complete_date", contractitem.complete_date)
//            bundle.putString("amount", contractitem.long_Id)
//            bundle.putString("unit_price", contractitem.long_Id)
//            bundle.putString("quantity", contractitem.long_Id)
////
////            val ViewContractItemsFragment = ViewSalesOrderItemsFragment()
////            ViewContractItemsFragment.setArguments(bundle)
////
////            val fragmentManager =(parentFragment.activity as MainActivity?)?.supportFragmentManager
////            var fr =fragmentManager?.beginTransaction()
////
////            fr?.add(R.id.nav_host_fragment_activity_main, ViewContractItemsFragment,"ViewContractItems")
////            fr?.addToBackStack(null)
////            fr?.commit()
//        })
    }

    override fun getItemCount(): Int {
        return mSalesOrderItems.size

    }


}