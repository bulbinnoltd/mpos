package com.example.salesdept.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.salesdept.MainActivity

import com.example.salesdept.R
import com.example.salesdept.item.OrderItem


class OrderItemWithCounterAdapter (private val  mOrderItems: List<OrderItem>, private val parentFragment: Fragment) : RecyclerView.Adapter<OrderItemWithCounterAdapter.ViewHolder>()
{

    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val item_name = itemView.findViewById<TextView>(R.id.item_name)
        val item_price = itemView.findViewById<TextView>(R.id.item_price)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val blogView = inflater.inflate(R.layout.item_order_counter, parent, false)
        return ViewHolder(blogView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        viewHolder.setIsRecyclable(false)
        val orderitem: OrderItem = mOrderItems.get(position)
        // Set item views based on your views and data model
        val item_name = viewHolder.item_name
        val item_price = viewHolder.item_price

        item_name.setText(orderitem.name)
        item_price.setText( "$".plus(orderitem.price)!!)


    }

    override fun getItemCount(): Int {
        return mOrderItems.size

    }


}