package com.example.salesdept.ui.dashboard

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.SnapHelper

import com.example.salesdept.R
import com.example.salesdept.adapter.CalendarAdapter
import com.example.salesdept.adapter.CalendarDateModel
import com.example.salesdept.adapter.HorizontalItemDecoration
import com.example.salesdept.databinding.FragmentDashboardBinding

import java.text.SimpleDateFormat

import java.util.*

class DashboardFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        val text = root.findViewById(R.id.text_dashboard) as TextView
        text.text = "This is dashboard Fragment"

//        return view
        return root
    }


}