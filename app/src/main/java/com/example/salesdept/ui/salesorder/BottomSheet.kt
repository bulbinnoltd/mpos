package com.example.salesdept.ui.salesorder

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.salesdept.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomSheet  : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_bottom_sheet_dialog, container, false)

    companion object {
        const val TAG = "ModalBottomSheet"
    }
}