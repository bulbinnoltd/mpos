package com.example.salesdept.ui.salesorder

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.salesdept.MainActivity
import com.example.salesdept.R
import com.example.salesdept.adapter.OrderItemAdapter
import com.example.salesdept.item.OrderItem
import kotlinx.android.synthetic.main.fragment_home.*

class ViewSalesOrderItemsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_view_salesorder_item, container, false)

        val backButton = root.findViewById<View>(R.id.back_button) as ImageView
        backButton.setOnClickListener() {
            activity?.onBackPressed()

        }

        val itemRecyclerView = root.findViewById<View>(R.id.recyclerView) as RecyclerView
        val tempItem = OrderItem(1,"揭蓋式塑膠儲物箱連蓋(⼤)-透明⽩⾊",1,240.0)
        var orderitems: ArrayList<OrderItem>? = ArrayList()
        if (orderitems != null) {
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
        }
        if (orderitems == null)
            orderitems = ArrayList<OrderItem>()

        val order_adapter =
            parentFragment?.let { OrderItemAdapter(orderitems, it) }
        itemRecyclerView.adapter = order_adapter
        itemRecyclerView.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        val editButton = root.findViewById<View>(R.id.order_edit_icon) as ImageView
        editButton.isClickable=true
        editButton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_viewsalesorderFragement_to_editSalesorderFragement))

//        editButton.setOnClickListener() {
//            Log.d("note", "EDIT BUTTON clicked")
//            val bundle = Bundle()
//            bundle.putString("order_time", "temp")
//
//            val EditSalesOrderItemsFragment = EditSalesOrderItemsFragment()
//            EditSalesOrderItemsFragment.setArguments(bundle)
//
//            var fr =(parentFragment?.activity as MainActivity?)?.supportFragmentManager?.beginTransaction()
//            fr?.addToBackStack(null)
//            fr?.add(R.id.nav_host_fragment_activity_main, EditSalesOrderItemsFragment)
//            fr?.commit()
//            Log.d("note", "2.EDIT BUTTON clicked")
//
//        }

        return root

    }



}