package com.example.salesdept.ui.salesorder


import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.example.salesdept.MainActivity
import com.example.salesdept.R
import com.example.salesdept.adapter.SalesOrderItemAdapter
import com.example.salesdept.item.SalesOrderItem
import com.example.salesdept.adapter.CalendarAdapter
import com.example.salesdept.adapter.CalendarDateModel
import com.example.salesdept.adapter.HorizontalItemDecoration
import com.example.salesdept.databinding.FragmentSalesorderBinding
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

import com.michalsvec.singlerowcalendar.calendar.CalendarChangesObserver
import com.michalsvec.singlerowcalendar.calendar.CalendarViewManager
import com.michalsvec.singlerowcalendar.calendar.SingleRowCalendar
import com.michalsvec.singlerowcalendar.calendar.SingleRowCalendarAdapter
import com.michalsvec.singlerowcalendar.selection.CalendarSelectionManager
import com.michalsvec.singlerowcalendar.utils.DateUtils
import kotlinx.android.synthetic.main.calendar_item.view.*
import kotlinx.android.synthetic.main.fragment_salesorder.*

class SalesorderFragment : Fragment() {

    private val calendar = Calendar.getInstance()
    private var currentMonth = 0

    private val sdf = SimpleDateFormat("MMMM yyyy", Locale.TRADITIONAL_CHINESE)
    private val cal = Calendar.getInstance(Locale.TRADITIONAL_CHINESE)
    private val currentDate = Calendar.getInstance(Locale.TRADITIONAL_CHINESE)
    private val dates = java.util.ArrayList<Date>()
    private lateinit var adapter: CalendarAdapter

    private val calendarList2 = java.util.ArrayList<CalendarDateModel>()

    private var _binding: FragmentSalesorderBinding? = null
    private val binding get() = _binding!!

    lateinit var myCalendarViewManager : CalendarViewManager
    lateinit var myCalendarChangesObserver : CalendarChangesObserver
    lateinit var mySelectionManager : CalendarSelectionManager

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        val args = arguments
//
//    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        val root = inflater.inflate(R.layout.fragment_salesorder, container, false)
        _binding = FragmentSalesorderBinding.inflate(inflater, container, false)
        val view = binding.root

        setUpAdapter()
        setUpClickListener()
        setUpCalendar()

        var actionBar = (activity as MainActivity).supportActionBar
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        val order_listview =view.findViewById<View>(R.id.orderitems_listview) as RecyclerView


    var arraylist = ArrayList<String>()
        arraylist.add("1")
        val tempItem = SalesOrderItem(246,26483765,"沙⽥區沙⽥正街18 號新城市廣場","Ms. Wong","1","h3141343242344515423423926","沙⽥區","10/03/2022","1",arraylist)

        var orderitems: ArrayList<SalesOrderItem>? = ArrayList()
        if (orderitems != null) {
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem

        }
        if (orderitems == null)
            orderitems = ArrayList<SalesOrderItem>()

        val order_adapter =
            parentFragment?.let { SalesOrderItemAdapter(orderitems, it) }
        order_listview.adapter = order_adapter
        order_listview.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        // calendar
        setHasOptionsMenu(true)

/*
//Source: Single row calendar  https://misosvec01.medium.com/single-row-horizontal-calendar-has-never-been-easier-6ad989fafee8


//        var calendar_view = root.findViewById(R.id.main_single_row_calendar) as SingleRowCalendar
//        var btnLeft = root.findViewById(R.id.btnLeft) as Button
//        var btnRight = root.findViewById(R.id.btnRight)as Button
//        calendar.time = Date()
//        currentMonth = calendar[Calendar.MONTH]
//
//        // calendar view manager is responsible for our displaying logic
//       val myCalendarViewManager = object: CalendarViewManager {
//            override fun setCalendarViewResourceId(
//                position: Int,
//                date: Date,
//                isSelected: Boolean
//            ): Int {
//                return R.layout.calendar_item
//                // set date to calendar according to position where we are
//                val cal = Calendar.getInstance()
//                cal.time = date
//                // if item is selected we return this layout items
//                // in this example monday, wednesday and friday will have special item views and other days
//                // will be using basic item view
//                return if (isSelected)
//                    when (cal[Calendar.DAY_OF_WEEK]) {
//                        Calendar.MONDAY -> R.layout.first_special_selected_calendar_item
//                        Calendar.WEDNESDAY -> R.layout.second_special_selected_calendar_item
//                        Calendar.FRIDAY -> R.layout.third_special_selected_calendar_item
//                        else -> R.layout.selected_calendar_item
//                    }
//                else
//                // here we return items which are not selected
//                    when (cal[Calendar.DAY_OF_WEEK]) {
//                        Calendar.MONDAY -> R.layout.first_special_calendar_item
//                        Calendar.WEDNESDAY -> R.layout.second_special_calendar_item
//                        Calendar.FRIDAY -> R.layout.third_special_calendar_item
//                        else -> R.layout.calendar_item
//                    }
//
//                 NOTE: if we don't want to do it this way, we can simply change color of background
//                 in bindDataToCalendarView method
//            }
//
//            override fun bindDataToCalendarView(
//                holder: SingleRowCalendarAdapter.CalendarViewHolder,
//                date: Date,
//                position: Int,
//                isSelected: Boolean
//            ) {
//                // using this method we can bind data to calendar view
//                // good practice is if all views in layout have same IDs in all item views
//                holder.itemView.tv_date_calendar_item.text = DateUtils.getDayNumber(date)
//                holder.itemView.tv_day_calendar_item.text = DateUtils.getDay3LettersName(date)
//
//            }
//        }
//        // using calendar changes observer we can track changes in calendar
//         val myCalendarChangesObserver = object : CalendarChangesObserver {
//            // you can override more methods, in this example we need only this one
//            override fun whenSelectionChanged(isSelected: Boolean, position: Int, date: Date) {
//                tvDate.text = "${DateUtils.getMonthName(date)}, ${DateUtils.getDayNumber(date)} "
//                tvDay.text = DateUtils.getDayName(date)
//                super.whenSelectionChanged(isSelected, position, date)
//            }
//        }
//
//        // selection manager is responsible for managing selection
//        val mySelectionManager = object : CalendarSelectionManager {
//            override fun canBeItemSelected(position: Int, date: Date): Boolean {
//                // set date to calendar according to position
//                val cal = Calendar.getInstance()
//                cal.time = date
//                //in this example sunday and saturday can't be selected, other item can be selected
//                return when (cal[Calendar.DAY_OF_WEEK]) {
//                    Calendar.SATURDAY -> false
//                    Calendar.SUNDAY -> false
//                    else -> true
//                }
//            }
//        }
//
//        // here we init our calendar, also you can set more properties if you need them
//        val singleRowCalendar = main_single_row_calendar.apply {
//              calendarViewManager = myCalendarViewManager
//              calendarChangesObserver = myCalendarChangesObserver
//              calendarSelectionManager = mySelectionManager
//            setDates(getFutureDatesOfCurrentMonth())
//            init()
//        }
//
//        btnRight.setOnClickListener {
//            singleRowCalendar.setDates(getDatesOfNextMonth())
//        }
//
//        btnLeft.setOnClickListener {
//            singleRowCalendar.setDates(getDatesOfPreviousMonth())
//        }
 */
        return view
    //        return root

    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.salesorders_menu, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.navigation_calendar -> {
            true
        }

        R.id.navigation_filter -> {
            true
        }
        android.R.id.home -> {
            if (activity != null) {
                requireActivity().onBackPressed()
            }
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }



    //1st example
    private fun setUpClickListener() {
        binding.ivCalendarNext.setOnClickListener {
            cal.add(Calendar.MONTH, 1)
            setUpCalendar()
        }
        binding.ivCalendarPrevious.setOnClickListener {
            cal.add(Calendar.MONTH, -1)
            if (cal == currentDate)
                setUpCalendar()
            else
                setUpCalendar()
        }
    }

    /**
     * Setting up adapter for recyclerview
     */
    private fun setUpAdapter() {
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.single_calendar_margin)
        binding.recyclerView.addItemDecoration(HorizontalItemDecoration(spacingInPixels))
        val snapHelper: SnapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(binding.recyclerView)
        adapter = CalendarAdapter { calendarDateModel: CalendarDateModel, position: Int ->
            calendarList2.forEachIndexed { index, calendarModel ->
                calendarModel.isSelected = index == position
            }
            adapter.setData(calendarList2)
        }

        binding.recyclerView.adapter = adapter
    }

    /**
     * Function to setup calendar for every month
     */
    private fun setUpCalendar() {
        val calendarList = java.util.ArrayList<CalendarDateModel>()
        binding.tvDateMonth.text = sdf.format(cal.time)
        val monthCalendar = cal.clone() as Calendar
        val maxDaysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        dates.clear()
        monthCalendar.set(Calendar.DAY_OF_MONTH, 1)
        while (dates.size < maxDaysInMonth) {
            dates.add(monthCalendar.time)
            calendarList.add(CalendarDateModel(monthCalendar.time))
            monthCalendar.add(Calendar.DAY_OF_MONTH, 1)
        }
        calendarList2.clear()
        calendarList2.addAll(calendarList)
        adapter.setData(calendarList)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null

    }

    //2nd example

    private fun getDatesOfNextMonth(): List<Date> {
        currentMonth++ // + because we want next month
        if (currentMonth == 12) {
            // we will switch to january of next year, when we reach last month of year
            calendar.set(Calendar.YEAR, calendar[Calendar.YEAR] + 1)
            currentMonth = 0 // 0 == january
        }
        return getDates(mutableListOf())
    }

    private fun getDatesOfPreviousMonth(): List<Date> {
        currentMonth-- // - because we want previous month
        if (currentMonth == -1) {
            // we will switch to december of previous year, when we reach first month of year
            calendar.set(Calendar.YEAR, calendar[Calendar.YEAR] - 1)
            currentMonth = 11 // 11 == december
        }
        return getDates(mutableListOf())
    }

    private fun getFutureDatesOfCurrentMonth(): List<Date> {
        // get all next dates of current month
        currentMonth = calendar[Calendar.MONTH]
        Log.d("note",currentMonth.toString())
        Log.d("note",getDates(mutableListOf()).toString())

        return getDates(mutableListOf())
    }


    private fun getDates(list: MutableList<Date>): List<Date> {
        // load dates of whole month
        calendar.set(Calendar.MONTH, currentMonth)
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        list.add(calendar.time)
        while (currentMonth == calendar[Calendar.MONTH]) {
            calendar.add(Calendar.DATE, +1)
            if (calendar[Calendar.MONTH] == currentMonth)
                list.add(calendar.time)
        }
        calendar.add(Calendar.DATE, -1)
        return list
    }


}