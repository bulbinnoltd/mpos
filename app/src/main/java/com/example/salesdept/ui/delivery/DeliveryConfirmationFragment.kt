package com.example.salesdept.ui.delivery


import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.budiyev.android.codescanner.*
import com.example.salesdept.MainActivity
import com.example.salesdept.R


class DeliveryConfirmationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_deliveryconfirmation, container, false)
        (activity as MainActivity).supportActionBar?.title = getString(R.string.title_delivery_confirmation)
        setHasOptionsMenu(true)

        var actionBar = (activity as MainActivity).supportActionBar
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        val button_next = root.findViewById(R.id.button_finish) as Button
        button_next.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_DeliveryConfirmation_to_DeliveryOrderEdit))
        val textview_delivery_no = root.findViewById(R.id.textview_delivery_no) as TextView

        val deliveryNo = "1465151981911516"
        textview_delivery_no.text = "收貨編號: ${deliveryNo}"

        val textview_order_no = root.findViewById(R.id.textview_order_no) as TextView

        val orderNo = "#246"
        textview_order_no.text = "收貨編號: ${orderNo}"



        return root
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            if (activity != null) {
                requireActivity().onBackPressed()
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }


}