package com.example.salesdept.ui.salesorder

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.salesdept.MainActivity
import com.example.salesdept.R
import com.example.salesdept.adapter.OrderItemAdapter
import com.example.salesdept.adapter.OrderItemWithCounterAdapter
import com.example.salesdept.item.OrderItem
import java.lang.Exception

class EditSalesOrderItemsFragment : Fragment(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_edit_salesorder_item, container, false)

        val cancelButton = root.findViewById<View>(R.id.cancel_button) as Button
        cancelButton.setOnClickListener() {
            activity?.onBackPressed()
        }
        val save_button = root.findViewById<View>(R.id.save_button) as Button
        save_button.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_editsalesorderFragement_to_homeFragement))

        val itemRecyclerView = root.findViewById<View>(R.id.recyclerView) as RecyclerView
        val tempItem = OrderItem(1,"揭蓋式塑膠儲物箱連蓋(⼤)-透明⽩⾊",1,240.0)

        var orderitems: ArrayList<OrderItem>? = ArrayList()
        if (orderitems != null) {
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem

        }
        if (orderitems == null)
            orderitems = ArrayList<OrderItem>()

        val order_adapter =
            parentFragment?.let { OrderItemWithCounterAdapter(orderitems, it) }
        itemRecyclerView.adapter = order_adapter
        itemRecyclerView.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        return root

    }

}