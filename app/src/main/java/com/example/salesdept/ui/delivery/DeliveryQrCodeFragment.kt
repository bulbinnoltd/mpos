package com.example.salesdept.ui.delivery

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.budiyev.android.codescanner.*
import com.example.salesdept.MainActivity
import com.example.salesdept.R


class DeliveryQrCodeFragment : Fragment() {
    private lateinit var codeScanner: CodeScanner


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_deliveryqrcode, container, false)
//        val text = root.findViewById(R.id.text_placeorder) as TextView
//        text.text = "This is Place order Fragment"
        (activity as AppCompatActivity?)!!.supportActionBar?.title= getString(R.string.title_qrscan)

        checkPermission(Manifest.permission.CAMERA,101)
        setHasOptionsMenu(true);

        var actionBar = (activity as MainActivity).supportActionBar
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        super.onCreate(savedInstanceState)
//        findNavController().popBackStack()
//        val back_button = root.findViewById(R.id.back_button) as ImageView
//        back_button.setOnClickListener {
//            requireActivity().onBackPressed()
//        }

        val scan_button =root.findViewById(R.id.scan_button) as Button
        scan_button.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_DeliveryQRCodeFragment_to_DeliveryConfirmation))

        return root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            if (activity != null) {
                requireActivity().onBackPressed()
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val scannerView = view.findViewById<CodeScannerView>(R.id.scanner_view)
        val scanButton = view.findViewById(R.id.scan_button) as Button
        var scanresult = ""
        val activity = requireActivity()
        codeScanner = CodeScanner(activity, scannerView)
        codeScanner.camera = CodeScanner.CAMERA_BACK
        codeScanner.formats = CodeScanner.ALL_FORMATS

        codeScanner.autoFocusMode = AutoFocusMode.SAFE
        codeScanner.scanMode = ScanMode.SINGLE
        codeScanner.isAutoFocusEnabled = true

        codeScanner.decodeCallback = DecodeCallback {
            activity.runOnUiThread {
                Toast.makeText(activity, "Scan result: ${it.text}", Toast.LENGTH_LONG).show()
                scanresult = it.text
            }
        }

        codeScanner.errorCallback = ErrorCallback {
            activity.runOnUiThread {
                Toast.makeText(activity, "Camera initialization error: ${it.message}", Toast.LENGTH_LONG).show()
            }
        }

//        scanButton.setOnClickListener {
        scannerView.setOnClickListener {
            codeScanner.startPreview()
            Navigation.createNavigateOnClickListener(R.id.action_placeOrderFragment_to_placeOrderFragement_1)

        }

    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

    private fun checkPermission(permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(
                activity as MainActivity,
                permission
            ) == PackageManager.PERMISSION_DENIED
        ) {
            // Requesting the permission
            ActivityCompat.requestPermissions(
                activity as MainActivity,
                arrayOf(permission),
                requestCode
            )
        } else {
            Log.d("Permission", "Camera permission granted")
        }
    }
}