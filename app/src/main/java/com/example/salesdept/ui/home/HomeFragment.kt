package com.example.salesdept.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.Navigation
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import com.example.salesdept.MainActivity
import com.example.salesdept.R
import com.example.salesdept.ui.placeorder.PlaceorderFragment

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val button_salesorder = root.findViewById(R.id.button_salesorder) as Button
        val button_placeorder = root.findViewById(R.id.button_placeorder) as Button
        val button_delivery_check = root.findViewById(R.id.button_delivery_check) as Button
        val button_testonly = root.findViewById(R.id.title_salesorder) as TextView

//        button_testonly.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_placeOrderFragement_2));
        button_salesorder.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_salesOrderFragment));
        button_placeorder.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_placeOrderFragment));
        button_delivery_check.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_deliveryFragment))

//        button_salesorder.setOnClickListener(){
//            Log.d("e","clicked")
//
////            findNavController().navigate(R.id.navigation_placeorder);
//
//            val transaction = activity?.supportFragmentManager?.beginTransaction()
//            transaction?.replace(R.id.fragment_home, PlaceorderFragment(),"fragment_home")
////            transaction?.addToBackStack(null)
////            transaction?.disallowAddToBackStack()
//            transaction?.commit()
//
//
//        }

        return root

    }


}