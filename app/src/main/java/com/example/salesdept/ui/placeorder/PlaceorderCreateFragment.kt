package com.example.salesdept.ui.placeorder

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.budiyev.android.codescanner.*
import com.example.salesdept.MainActivity
import com.example.salesdept.R


class PlaceorderCreateFragment : Fragment() {
    private lateinit var codeScanner: CodeScanner


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_placeorder_1, container, false)
        (activity as MainActivity).supportActionBar?.title = getString(R.string.text_build_order)
        setHasOptionsMenu(true)

        var actionBar = (activity as MainActivity).supportActionBar
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        val button_next = root.findViewById(R.id.next_button) as Button
        button_next.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_placeOrderFragment_1_to_placeOrderFragement_2))


        val itemsColor = listOf("Black", "White", "Brown", "Blue")
        val adapterColor = ArrayAdapter(requireContext(), R.layout.list_item, itemsColor)
        val prod_color_value = root.findViewById<AutoCompleteTextView>(R.id.prod_color_value)
        prod_color_value?.setAdapter(adapterColor)

        val itemsClass = listOf("Sofas", "Lighting", "Storage", "Beds","Desks & Chairs")
        val adapterClass = ArrayAdapter(requireContext(), R.layout.list_item, itemsClass)
        val prod_color_class = root.findViewById<AutoCompleteTextView>(R.id.prod_class_value)
        prod_color_class?.setAdapter(adapterClass)

        return root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            if (activity != null) {
                requireActivity().onBackPressed()
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }


}