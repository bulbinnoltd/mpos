package com.example.salesdept.ui.placeorder


import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.budiyev.android.codescanner.*
import com.example.salesdept.MainActivity
import com.example.salesdept.R


class PlaceorderPaymentDetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_placeorder_4, container, false)
        (activity as MainActivity).supportActionBar?.title = getString(R.string.text_build_order)
        setHasOptionsMenu(true)

        var actionBar = (activity as MainActivity).supportActionBar
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        val button_next = root.findViewById(R.id.button_1) as Button
        button_next.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_placeOrderFragment_4_to_placeOrderFragement_5))

        return root
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            if (activity != null) {
                requireActivity().onBackPressed()
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }


}