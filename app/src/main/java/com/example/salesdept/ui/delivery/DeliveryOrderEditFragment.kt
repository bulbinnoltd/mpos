package com.example.salesdept.ui.delivery


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.OnFocusChangeListener
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.budiyev.android.codescanner.*
import com.example.salesdept.MainActivity
import com.example.salesdept.R
import com.example.salesdept.adapter.OrderItemAdapter
import com.example.salesdept.item.OrderItem

class DeliveryOrderEditFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_delivery_edit, container, false)
        (activity as MainActivity).supportActionBar?.title = getString(R.string.title_delivery_confirmation)
        setHasOptionsMenu(true)

        var actionBar = (activity as MainActivity).supportActionBar
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        val button_next = root.findViewById(R.id.button_finish) as Button
        button_next.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_DeliveryOrderEdit_to_Home))
        val textview_delivery_orderno = root.findViewById(R.id.textview_delivery_orderno) as TextView
        textview_delivery_orderno.text= getString(R.string.text_delivery_orderno).plus(":")

        val itemRecyclerView = root.findViewById<View>(R.id.recyclerView) as RecyclerView

        val tempItem = OrderItem(1,"揭蓋式塑膠儲物箱連蓋(⼤)-透明⽩⾊",1,240.0)

        var orderitems: ArrayList<OrderItem>? = ArrayList()
        if (orderitems != null) {
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem
            orderitems+=tempItem

        }
        if (orderitems == null)
            orderitems = ArrayList<OrderItem>()

        val order_adapter =
            parentFragment?.let { OrderItemAdapter(orderitems, it) }
        itemRecyclerView.adapter = order_adapter
        itemRecyclerView.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        return root
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            if (activity != null) {
                requireActivity().onBackPressed()
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }


}